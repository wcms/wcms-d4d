# UW Cookiecutter WCMS3

Internal documentation (requires login): https://uwaterloo.atlassian.net/wiki/spaces/ISTWCMS/pages/42964942870/Docker+for+Drupal+d4d

## User default context
To avoid typing your name and email you can use default context.
Update your ~/.cookiecutterrc file to be:
```shell
default_context:
    name: "John Doe"
    email: "jdoe@uwaterloo.ca"
    os: "mac"

abbreviations:
    uw: https://git.uwaterloo.ca/wcms/wcms-d4d.git
```
Key-values in default_context will be used as defaults, in this case mac will be used as default option for os prompt. You can update any value from cookiecutter.json here to be used as default.

If you want to add some user default, just add something like:
```shell
default_context:
    name: "John Doe"
    email: "jdoe@uwaterloo.ca"
    os: "mac"
    drupal: "10"
    php: "8.2"
```
This will make sure that Drupal 10 and PHP 8.2 are defaults.
### How to use it?
Simply running `cookiecutter uw` will create new project structure, pulling defaults from your .cookiecutterrc file.

You can provide url so that command looks like:
`cookiecutter https://git.uwaterloo.ca/wcms/wcms-d4d.git`

Either case will prompt you for project name, port, and other details. Some defaults are provided.

Main difference between linux and mac version is that mac uses mutagen to sync files between host & docker. Linux has native docker support and mutagen is not required.
