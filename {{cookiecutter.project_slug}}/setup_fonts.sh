#!/bin/bash

if [[ -d html/web ]]; then
    git clone https://git.uwaterloo.ca/wcms/config --branch wcms-3 html/web/config
    cp -r ./html/web/config/fonts ./html/web
else
    echo "Error: missing folders - html/web!"
    exit 1;
fi
