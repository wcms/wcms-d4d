# Git function to return branch/tag name for the PS1.
function parse_git_branch() {
	BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
	if [ ! "${BRANCH}" == "" ]
	then
		STAT=`parse_git_dirty`
		echo "[${BRANCH}${STAT}]"
	else
		echo ""
	fi
}

# get current status of git repo
function parse_git_dirty {
	status=`git status 2>&1 | tee`
	dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
	untracked=`echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?"`
	ahead=`echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?"`
	newfile=`echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?"`
	renamed=`echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?"`
	deleted=`echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?"`
	bits=''
	if [ "${renamed}" == "0" ]; then
		bits=">${bits}"
	fi
	if [ "${ahead}" == "0" ]; then
		bits="*${bits}"
	fi
	if [ "${newfile}" == "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" == "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" == "0" ]; then
		bits="x${bits}"
	fi
	if [ "${dirty}" == "0" ]; then
		bits="!${bits}"
	fi
	if [ ! "${bits}" == "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
}

# Prompt
PS1="\n\[\033[4;34m\]$DRUSH_OPTIONS_URI\n\[\033[0;32m\]\[\033[0m\033[0;32m\]\u\[\033[0;35m\]@\[\033[0;35m\]${WODBY_APP_NAME:-php}.${WODBY_ENVIRONMENT_NAME:-container} \[\033[0;32m\]\w\[\033[0;33m\] \`parse_git_branch\` \n\[\033[0;32m\]└─\[\033[0m\033[0;32m\]▶\[\033[0m\033[0;32m\]\[\033[0m\] "

# Global helper aliases
alias ll='ls -alhF --color=auto'
alias ..='cd ../'
alias ..2='cd ../../'
alias ..3='cd ../../../'
alias ..4='cd ../../../../'
alias ..5='cd ../../../../../'
alias ..6='cd ../../../../../../'
alias ..7='cd ../../../../../../../'
alias ..8='cd ../../../../../../../../'
alias storegitpw='git config --global credential.helper store'

# Drupal generic aliases
alias dsi='drush si --db-url=mysql://drupal:drupal@mariadb/drupal'
alias d9rp='composer create-project drupal/recommended-project:^9 ./'
alias d10rp='composer create-project drupal/recommended-project:^10 ./'
alias d11rp='composer create-project drupal/recommended-project:^11 ./'
alias dcr='drush cr'
alias duli='drush uli'

# WCMS 3 specific aliases
alias wcms3='git clone https://git.uwaterloo.ca/wcms/drupal-recommended-project.git .'
alias wcms='git clone https://git.uwaterloo.ca/wcms/drupal-recommended-project.git .'
alias si='drush si uw_base_profile --db-url=mysql://drupal:drupal@mariadb/drupal --account-name=wcmsadmin --account-pass=a --site-name="WCMS Test"'
alias d9uw='si -y'
alias wcmsrpsi='si -y'
alias runcs='phpcs --standard=Drupal,DrupalPractice --report-width=150 --extensions=php,module,inc,install,test,profile,theme,js,info,txt,md,scss,css,sh --ignore=*.min.js,*.min.css,libraries/* .'
alias runcsfix='phpcbf --standard=Drupal,DrupalPractice --report-width=150 --extensions=php,module,inc,install,test,profile,theme,js,info,txt,md,scss,css,sh --ignore=*.min.js,*.min.css,libraries/* .'
alias runcsphp='phpcs --standard=PHPCompatibility --runtime-set testVersion 7.4 --report-width=150 --extensions=php,module,inc,install,test,profile,theme .'
alias ics='composer global require "squizlabs/php_codesniffer=*" && composer global require drupal/coder && composer global require dealerdirect/phpcodesniffer-composer-installer'
alias runtests='cd /var/www/html/web/profiles/uw_base_profile && php run-tests.php http://nginx --suppress-deprecations && cd - >/dev/null'
alias wcmsconfig='if [[ -d /var/www/html/web ]]; then
    git clone https://git.uwaterloo.ca/wcms/config --branch wcms-3 /var/www/html/web/config
    cp -r /var/www/html/web/config/fonts /var/www/html/web
else
    echo "Error: missing folders - /var/www/html/web!"
    exit 1;
fi'
alias wcmsdev='if [[ -d /var/www/html/web/sites/default ]]; then
    cp /home/wodby/settings.* /var/www/html/web/sites/default
    cp /home/wodby/development* /var/www/html/web/sites/default
    sudo chmod 777 /var/www/html/web/sites/default/settings.php
    mkdir -p /var/www/html/web/sites/files/sync
    sudo chmod 777 /var/www/html/web/sites/files
else
    echo "Error: missing folders /var/www/html/web/sites/default!"
    exit 1
fi'
alias setup_site='wcmsconfig && wcmsdev'

# Drush specifics, this is for debugging CLI (drush).
alias dxdon='export XDEBUG_CONFIG="idekey=PHPSTORM"'
alias dxdoff='unset XDEBUG_CONFIG'

# UW git
alias uwgit='git config --global user.email "{{ cookiecutter.email }}" && git config --global user.name "{{ cookiecutter.name }}"'

# WCMS Tools
alias uwwcmstools='git clone https://git.uwaterloo.ca/wcms/uw_wcms_tools.git /home/wodby/uw_wcms_tools'
alias uwgen='cd /var/www/html/web/modules && git clone https://git.uwaterloo.ca/wcms/uw_wcms_gen.git && cd - > /dev/null'
alias preprelease='cd ~ &&  git clone https://git.uwaterloo.ca/wcms-automation/prep-release-code.git && mv prep-release-code/prep-release.sh . && rm -rf prep-release-code'

# This is for chromedriver.
alias startchrome='chromedriver --port=9515 --url-base=/wd/hub &'

# Quickly go to specific folders
alias project='cd /var/www/html'
alias profile='cd /var/www/html/web/profiles/uw_base_profile'
alias site='cd /var/www/html/web/sites/default'
alias theme='cd /var/www/html/web/profiles/uw_base_profile/themes/uw_fdsu_theme_resp'
# Make the ones that have subfolders take optional parameters.
# Has to be a function to make the argument work.
function contrib() { cd /var/www/html/web/profiles/uw_base_profile/modules/contrib/$1; }
function custom() { cd /var/www/html/web/profiles/uw_base_profile/modules/custom/$1; }
function features() { cd /var/www/html/web/profiles/uw_base_profile/modules/features/$1; }
function themes() { cd /var/www/html/web/profiles/uw_base_profile/themes/$1; }
# Give these autocomplete.
{% raw %}
function _folder_completions() {
  if [ "${#COMP_WORDS[@]}" != "2" ]; then
    return
  fi
  cd /var/www/html/web/profiles/uw_base_profile/$1
  options_list=$(ls -d */ | cut -f1 -d'/' | tr '\n' ' ')
  COMPREPLY=($(compgen -W "$options_list" "${COMP_WORDS[1]}"))
}
{% endraw %}
function _contrib_folder_completion() {
  _folder_completions modules/contrib
}
function _custom_folder_completion() {
  _folder_completions modules/custom
}
function _features_folder_completion() {
  _folder_completions modules/features
}
function _themes_folder_completion() {
  _folder_completions themes
}
complete -F _contrib_folder_completion contrib
complete -F _custom_folder_completion custom
complete -F _features_folder_completion features
complete -F _themes_folder_completion themes

# Ohana specific
alias ohana='cd /var/www/html/web/profiles/uw_base_profile/modules/custom/uw_wcms_ohana'
alias runohana='cd /var/www/html/web/profiles/uw_base_profile/modules/custom/uw_wcms_ohana/ohana && nvm use && npm run watch'
alias buildohana='cd /var/www/html/web/profiles/uw_base_profile/modules/custom/uw_wcms_ohana/ohana && nvm use && npm run build && cd - >/dev/null'

# Setting NVM
[ -s "$NVM_DIR/nvm.sh" ] && source "$NVM_DIR/nvm.sh"

# Setting up xdebug.
{% raw %}

GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

function xfprod() {
    if [ -f "/var/www/html/composer.json" ] && [ ! -d "/var/www/html/private/files" ]; then
        echo -e "File ${GREEN}composer.json${NC} detected and missing private folder, creating it ..."
        mkdir -p /var/www/html/private/files
    elif [ -f "/var/www/html/composer.json" ]; then
        echo -e "Private folder ${GREEN}exist${NC}. Skipping creating it."
    else
        echo -e "${RED}Drupal (composer.json)${NC} not found. Rerun this command once there is Drupal installed."
        return
    fi

    if [ -f "/var/www/html/web/sites/default/settings.php" ]; then
        if ! grep -q "wcms-common" /var/www/html/web/sites/default/settings.php; then
            echo -e "Applying changes to settings.php file."
            cat /home/wodby/wcms-common.txt | sudo tee -a /var/www/html/web/sites/default/settings.php > /dev/null
        else
            echo -e "Prod changes ${GREEN}already applied${NC}. Skipping."
        fi
    else
        echo -e "File ${RED}settings.php${NC} not found. Install the site, and rerun this command."
    fi
}

function xfdebug() {
    xfprod

    if [ -f "/var/www/html/composer.json" ]; then
        if [ -d /var/www/html/web/sites/default ] && [ -f "/var/www/html/web/sites/default/settings.php" ]; then
            if [ ! -f /var/www/html/web/sites/default/development.services.yml ]; then
                echo "Enabling development services."
                sudo cp ~/development.services.yml /var/www/html/web/sites/default
                sudo chown wodby:wodby /var/www/html/web/sites/default/development.services.yml
            else
                echo -e "File: ${GREEN}development.services.yml${NC} already exists. Skipping."
            fi

            if [ ! -f /var/www/html/web/sites/default/settings.local.php ]; then
                echo "Enabling settings local file."
                sudo cp ~/settings.local.php /var/www/html/web/sites/default
                sudo chown wodby:wodby /var/www/html/web/sites/default/settings.local.php
            else
                echo -e "File: ${GREEN}settings.local.php${NC} already exists. Skipping."
            fi

            if ! grep -q "wcms-debug" /var/www/html/web/sites/default/settings.php; then
                echo "Updating settings.php file with xdebug."
                cat ~/wcms-debug.txt | sudo tee -a /var/www/html/web/sites/default/settings.php > /dev/null
            else
                echo -e  "Debug settings ${GREEN}already applied${NC}. Skipping."
            fi
        else
            echo -e "Folder ${RED}/var/www/html/web/sites/default${NC} not found or missing ${RED}settings.php${NC} file. Install the site and rerun this command."
        fi
    fi
}
{% endraw %}
alias prod=xfprod;
alias debug=xfdebug;

# Welcome message. Because of docker we can't use /etc/motd output. Instead we use echos like this:
echo "####################################################################"
echo ""
echo "█████   ███   █████   █████████  ██████   ██████  █████████"
echo "░░███   ░███  ░░███   ███░░░░░███░░██████ ██████  ███░░░░░███"
echo "░███   ░███   ░███  ███     ░░░  ░███░█████░███ ░███    ░░░"
echo "░███   ░███   ░███ ░███          ░███░░███ ░███ ░░█████████"
echo "░░███  █████  ███  ░███          ░███ ░░░  ░███  ░░░░░░░░███"
echo " ░░░█████░█████░   ░░███     ███ ░███      ░███  ███    ░███"
echo "   ░░███ ░░███      ░░█████████  █████     █████░░█████████"
echo "    ░░░   ░░░        ░░░░░░░░░  ░░░░░     ░░░░░  ░░░░░░░░░"
echo ""
echo "####################################################################"
echo ""
echo "To prepare the environment for WCMS3 development, run the following commands:"
echo "-----------------------------------------------------------------------------"
echo " - 'wcms3' or 'wcms' (clones WCMS recommended project into the current folder)"
echo " - './rebuild.sh' (builds profile)"
echo " - 'wcmsdev' (sets up site for development)"
echo " - 'si -y' (installs site)"
echo " - 'storegitpw' (optional - stores git credentials once entered once)"
echo " - 'prod' (creates private folder, loads all config files)"
echo " - 'debug' (calls prod, and then prepares xdebug stuff)"
echo ""
export NODE_OPTIONS='--no-experimental-fetch'
