#!/bin/bash

# Setup config repo, and fonts.
bash ./setup_fonts.sh

# Copy files for local development, this disables cache,
# and enable development resources.
if [[ -d html/web/sites/default ]]; then
    cp images/settings.* html/web/sites/default
    cp images/development* html/web/sites/default
    sudo chmod 777 html/web/sites/default/settings.php
    mkdir -p html/web/sites/files/sync
    sudo chmod 777 html/web/sites/files
else
    echo "Error: missing folders html/web/sites/default!"
    exit 1
fi
