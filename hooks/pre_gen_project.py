import re
import socket
import sys

PROJECT_SLUG_REGEX = r"^[a-zA-Z][_a-zA-Z0-9]+$"

project_slug = "{{ cookiecutter.project_slug }}"

if not re.match(PROJECT_SLUG_REGEX, project_slug):
    print(f"ERROR: Project slug - {project_slug} is not valid slug.")
    sys.exit(1)

PROJECT_PORT_MIN = 6000
PROJECT_PORT_MAX = 8999
OHANA_PORT_MIN = 1000
OHANA_PORT_MAX = 5999

project_port = int("{{ cookiecutter.project_port }}")
ohana_port = int("{{ cookiecutter.ohana_port }}")

if not OHANA_PORT_MIN <= ohana_port <= OHANA_PORT_MAX:
    print(f"ERROR: Ohana port needs to be between {OHANA_PORT_MIN} and {OHANA_PORT_MAX}.")
    sys.exit(1)

if not PROJECT_PORT_MIN <= project_port <= PROJECT_PORT_MAX:
    print(f"ERROR: Port needs to be between {PROJECT_PORT_MIN} and {PROJECT_PORT_MAX}.")
    sys.exit(1)


a_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
a_socket.settimeout(2)
location = ("127.0.0.1", project_port)

if a_socket.connect_ex(location) == 0:
    a_socket.close()
    print(f"ERROR: Port is already in use. Pick another port.")
    sys.exit(1)

a_socket.close()

# Validate that name, username and email are not empty

name = "{{ cookiecutter.name }}"
email = "{{ cookiecutter.email }}"

if not all([name, email]):
    print(f"ERROR: Name and/or email values are empty!")
    sys.exit(1)
